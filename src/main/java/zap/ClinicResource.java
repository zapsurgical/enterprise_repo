package zap;

import Data.Site;
import Database.DatabaseDriver;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

@Path("clinic")
public class ClinicResource {

    @GET
    @Path("sites")
    @Produces(MediaType.TEXT_PLAIN)
    public String getClinicSites() throws SQLException {
        List<Site> result = DatabaseDriver.callStoredProcedure(new DatabaseDriver.ResultSetHandler<List<Site>>() {
            @Override
            public List<Site> process(ResultSet result) throws Exception {
                List<Site> sites = new LinkedList<>();
                while(result.next()) {
                    String siteUuid = result.getString("a");
                    String name = result.getString("b");
                    String description = result.getString("c");
                    sites.add(new Site(siteUuid, name, description));
                }
                return sites;
            }
        }, "querysitesinfo");
        return result.toString();
    }

}
