package zap;

import Database.DatabaseDriver;
import Data.System;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

@Path("system")
public class SystemResource {

    @GET
    @Path("info")
    @Produces(MediaType.TEXT_PLAIN)
    public String getSystemInfo() {
        List<System> result = DatabaseDriver.callStoredProcedure(new DatabaseDriver.ResultSetHandler<List<System>>() {
            @Override
            public List<System> process(ResultSet result) throws Exception {
                List<System> systems = new LinkedList<>();
                while(result.next()) {
                    String uuid = result.getString("Uuid");
                    String name = result.getString("Name");
                    String hardwareKey = result.getString("HardwareKey");
                    String siteUuid = result.getString("SiteUuid");
                    systems.add(new System(uuid, name, hardwareKey, siteUuid));
                }
                return systems;
            }
        }, "querysysteminfo", null, null);
        return result.toString();
    }

}
