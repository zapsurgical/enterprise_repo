package Data;

public class Site {

    private String siteUuid;
    private String name;
    private String description;

    public Site() {
        this(null, null, null);
    }

    public Site(String siteUuid, String name, String description) {
        this.siteUuid = siteUuid;
        this.name = name;
        this.description = description;
    }

    public String getSiteUuid() {
        return siteUuid;
    }

    public void setSiteUuid(String siteUuid) {
        this.siteUuid = siteUuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Site{" +
                "siteUuid='" + siteUuid + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

}
