package Data;

public class System {

    private String uuid;
    private String name;
    private String harwareKey;
    private String siteUuid;

    public System() {
        this(null, null, null, null);
    }

    public System(String uuid, String name, String harwareKey, String siteUuid) {
        this.uuid = uuid;
        this.name = name;
        this.harwareKey = harwareKey;
        this.siteUuid = siteUuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHarwareKey() {
        return harwareKey;
    }

    public void setHarwareKey(String harwareKey) {
        this.harwareKey = harwareKey;
    }

    public String getSiteUuid() {
        return siteUuid;
    }

    public void setSiteUuid(String siteUuid) {
        this.siteUuid = siteUuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "SystemResource{" +
                "uuid='" + uuid + '\'' +
                ", name='" + name + '\'' +
                ", harwareKey='" + harwareKey + '\'' +
                ", siteUuid='" + siteUuid + '\'' +
                '}';
    }

}