package Database;

import org.apache.commons.dbcp2.BasicDataSource;
import Exception.ZException;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

public class DatabaseDriver {

    private static final String DRIVER_NAME = "org.postgresql.Driver";
    private static final String URL = "jdbc:postgresql://zapenterprise.c7li4bg7dfvt.us-west-2.rds.amazonaws.com/ZapEnterprise";
    private static final String USER = "ZapAdmin";
    private static final String PASSWORD = "Surgical#1";

    private static BasicDataSource pool = null;

    private static Connection getConnection() {
        try {
            if (pool == null) {
                initializeDataSource();
            }
            return pool.getConnection();
        } catch (Exception ex) {
            throw new ZException(ex, "Get Connection Failed");
        }
    }

    public interface ResultSetHandler<T> {
        T process(ResultSet result) throws Exception;
    }

    public static <T> T callStoredProcedure(ResultSetHandler<T> handler, String name, Object... args) {
        Connection conn = DatabaseDriver.getConnection();
        StringBuilder builder = new StringBuilder();
        builder.append("{call \"Zapsurgical\".");
        builder.append(name);
        builder.append('(');
        for (int i = 0; i < args.length; ++i) {
            if (i != 0) {
                builder.append(", ");
            }
            builder.append('?');
        }
        builder.append(")}");
        T outcome = null;
        ResultSet set;
        try {
            CallableStatement st = conn.prepareCall(builder.toString());
            for (int i = 0; i < args.length; ++i) {
                st.setObject(i + 1, args[i]);
            }
            set = st.executeQuery();
        } catch (Exception ex) {
            throw new ZException(ex, "Execute Stored Procedure Failed");
        }
        try {
            if (handler != null) outcome = handler.process(set);
        } catch (Exception ex) {
            throw new ZException(ex, "Process Result Failed");
        }
        try {
            conn.close();
        } catch (Exception ex) {
            System.out.println("Close Connection Failed");
            ex.printStackTrace();
        }
        return outcome;
    }

    public static void callStoredProcedure(String name, Object... args) {
        callStoredProcedure(null, name, args);
    }

    private static final int INITIAL_POOL_SIZE = 10;
    private static final int MAX_IDLE_CONN = 10;
    private static final int MIN_IDLE_CONN = 5;
    private static final int MAX_POOL_SIZE = 100;
    private static final int ABANDONED_CONN_TIMEOUT = 300; // 300 sec

    private static void initializeDataSource() {
        try {
            pool = new BasicDataSource();
            pool.setDriverClassName(DRIVER_NAME);
            pool.setUrl(URL);
            pool.setUsername(USER);
            pool.setPassword(PASSWORD);

            pool.setInitialSize(INITIAL_POOL_SIZE);
            pool.setMaxIdle(MAX_IDLE_CONN);
            pool.setMaxIdle(MIN_IDLE_CONN);
            pool.setMaxTotal(MAX_POOL_SIZE);

            pool.setRemoveAbandonedOnBorrow(true);
            pool.setRemoveAbandonedOnMaintenance(true);
            pool.setRemoveAbandonedTimeout(ABANDONED_CONN_TIMEOUT);
        } catch (Exception ex) {
            throw new ZException(ex, "Initialize Connection Pool Failed");
        }
    }

}
