package Exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.io.PrintWriter;

@Provider
public class ZExceptionMapper implements ExceptionMapper<ZException> {

    @Override
    public Response toResponse(ZException e) {
        e.printStackTrace();
        return Response.serverError().build();
    }

}
