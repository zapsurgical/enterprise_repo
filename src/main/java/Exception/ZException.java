package Exception;

import javax.ws.rs.WebApplicationException;

public class ZException extends RuntimeException {

    public ZException(Exception ex) {
        super(ex.toString(), ex.getCause());
    }

    public ZException(Exception ex, String message) {
        super(indentation(message, ex.toString()), ex.getCause());
    }

    private static String indentation(String message, String otherMessage) {
        return message + "\n\t" + otherMessage.replace("\n", "\n\t");
    }

}
